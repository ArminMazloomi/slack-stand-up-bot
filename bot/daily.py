from logic import fsm, cache
from bot import statics

_IDLE = fsm.State('IDLE')
_START = fsm.State('START')
_YESTERDAY = fsm.State('YESTERDAY')
_TODAY = fsm.State('TODAY')
_BLOCKER = fsm.State('BLOCKER')
_DONE = fsm.State('DONE')

# main path edges
_IDLE.add_handler('alarm_', _START)
_START.add_handler('begin', _YESTERDAY)
_YESTERDAY.add_handler('answer_yes', _TODAY)
_TODAY.add_handler('answer_tod', _BLOCKER)
_BLOCKER.add_handler('answer_blk', _DONE)
_BLOCKER.add_handler('no_blk', _DONE)

# edges for undoing to prev
_TODAY.add_handler('undo', _YESTERDAY)
_BLOCKER.add_handler('undo', _TODAY)

# edges for restarting done nodes
_DONE.add_handler('restart_', _IDLE)

# edges for expiring undone nodes
_START.add_handler('expired_', _IDLE)
_YESTERDAY.add_handler('expired_', _IDLE)
_TODAY.add_handler('expired_', _IDLE)
_BLOCKER.add_handler('expired_', _IDLE)

default_state = _IDLE

# add states to fsm
machine = fsm.StateMachine('daily-report', _IDLE)
machine.add_state(_IDLE, _START, _YESTERDAY, _TODAY, _BLOCKER, _DONE)

commands = ['alarm_', 'expired_', 'restart_']


def get_event_type(current_state, event_message):
    event_message = event_message.lower()
    # Commands
    if event_message in commands:
        return event_message

    # Logical commands
    if event_message == 'reset_':
        return 'expired_' if not (current_state == _DONE) else 'restart_'

    # User messages
    if event_message == statics.daily_request and current_state in [_DONE]:
        return 'alarm_'
    if event_message == statics.begin_request and current_state == _START:
        return 'begin'
    if event_message == statics.undo_request and current_state in [_YESTERDAY, _TODAY]:
        return 'undo'
    if current_state == _YESTERDAY:
        return 'answer_yes'
    if current_state == _TODAY:
        return 'answer_tod'
    if event_message == statics.no_blocker_request and current_state == _BLOCKER:
        return 'no_blk'
    if current_state == _BLOCKER:
        return 'answer_blk'


if __name__ == '__main__':
    from uuid import uuid4

    key = uuid4().__str__()
    machine1 = machine
    machine1.proceed('alarm')
    cache.save_fsm(key, machine1)
    machine2 = cache.retrieve_fsm(key)
    print(machine2.__repr__())
    machine2.proceed('y')
    print(machine2.__repr__())
    machine2.proceed('answer')
    print(machine2.__repr__())
    machine2.proceed('undo')
    print(machine2.__repr__())
