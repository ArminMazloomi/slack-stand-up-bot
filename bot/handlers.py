import datetime
from decouple import config
from slack_sdk.web import WebClient

from logic import cache
from bot import daily, db, adapter, statics

_BOT_ACCESS_TOKEN = config('BOT_ACCESS_TOKEN', default='', cast=str)
slack_web_client = WebClient(_BOT_ACCESS_TOKEN)

channel = config('DAILY_REPORT_CHANNEL_ID', default='', cast=str)


class DailyFSMHandler:

    @classmethod
    def handle_event(cls, *args, **kwargs):
        fsm_id = kwargs['user_id']
        machine = cache.retrieve_fsm(fsm_id)
        if not machine:
            cache.save_fsm(fsm_id, daily.machine)
            machine = daily.machine
        event_type = daily.get_event_type(machine.current_state, kwargs['text'])
        _, is_success = machine.proceed(event_type)
        if is_success:
            func = getattr(cls, event_type)
            func(*args, **kwargs)
        cache.save_fsm(fsm_id, machine)
        return machine

    @staticmethod
    def alarm_(*args, **kwargs):
        adapter.send_dm(slack_web_client, user_id=kwargs['user_id'], text=statics.alarm_response)

    @staticmethod
    def begin(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.create_date(user_id=kwargs['user_id'], date=today_str,
                       username=adapter.get_user_name(slack_web_client, kwargs['user_id']))
        adapter.send_dm(slack_web_client, user_id=kwargs['user_id'], text=statics.begin_response)

    @staticmethod
    def answer_yes(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.reports_col.update_one({'user_id': kwargs['user_id'], 'date': today_str},
                                  {'$set': {'yesterday_progress': kwargs['text']}})
        adapter.send_dm(slack_web_client, user_id=kwargs['user_id'], text=statics.answer_yes_response)

    @staticmethod
    def answer_tod(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.reports_col.update_one({'user_id': kwargs['user_id'], 'date': today_str},
                                  {'$set': {'today_progress': kwargs['text']}})
        adapter.send_dm(slack_web_client, user_id=kwargs['user_id'], text=statics.answer_tod_response)

    @staticmethod
    def _finish(*args, **kwargs):
        adapter.send_dm(slack_web_client, user_id=kwargs['user_id'], text=statics.answer_blk_response)
        slack_web_client.conversations_join(channel=channel)
        today_str = datetime.date.today().__str__()
        today_report = db.reports_col.find_one({'user_id': kwargs['user_id'], 'date': today_str},
                                               {'_id': 0})
        slack_web_client.chat_postMessage(channel=channel, text=today_report)

    @staticmethod
    def answer_blk(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.reports_col.update_one({'user_id': kwargs['user_id'], 'date': today_str},
                                  {'$set': {'blockers': kwargs['text']}})
        DailyFSMHandler._finish(*args, **kwargs)

    @staticmethod
    def no_blk(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.reports_col.update_one({'user_id': kwargs['user_id'], 'date': today_str},
                                  {'$unset': {'blockers': ''}})
        DailyFSMHandler._finish(*args, **kwargs)

    @staticmethod
    def undo(*args, **kwargs):
        pass

    @staticmethod
    def expired_(*args, **kwargs):
        today_str = datetime.date.today().__str__()
        db.reports_col.delete_one({'user_id': kwargs['user_id'], 'date': today_str})

    @staticmethod
    def restart_(*args, **kwargs):
        pass

