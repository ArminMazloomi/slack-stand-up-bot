import pymongo
from decouple import config

cl = pymongo.MongoClient(host=config('MONGO_HOST', default='localhost', cast=str),
                         port=config('MONOG_PORT', default=27017, cast=int))
reports_col = cl.yapaitekdb.reports


def create_date(user_id, date: str, username: str):
    rep = reports_col.find_one({'user_id': user_id, 'date': date})
    if rep:
        reports_col.update_one({'user_id': user_id, 'date': date}, {'$set': {'edited': True}})
    else:
        reports_col.insert_one({'user_id': user_id, 'date': date, 'edited': False, 'username': username})

