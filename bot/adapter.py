def send_dm(client, user_id, text):
    dm_channel = client.conversations_open(users=user_id)
    client.chat_postMessage(channel=dm_channel.get('channel')['id'], text=text)


def get_user_name(client, user_id):
    return '@{} '.format(client.users_info(user=user_id)['user']['name'])
