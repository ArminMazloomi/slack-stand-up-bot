class State:

    states = dict()

    def __init__(self, name: str):
        self.name = name
        self.handlers = dict()
        self.states[self.name] = self

    def add_handler(self, trigger, state):
        self.handlers[trigger] = state
        return state

    def next(self, trigger) -> None:
        try:
            return self.handlers[trigger]
        except KeyError:
            return None

    @classmethod
    def get(cls, name: str):
        if name in cls.states.keys():
            return cls.states[name], True
        else:
            return None, False

    def __str__(self):
        return self.name


class StateMachine:
    machines = dict()

    def __init__(self, name, idle_state: State):
        self.name = name
        self.states = {idle_state}
        self.current_state = idle_state
        self.machines[self.name] = self

    def add_state(self, *new_states):
        for state in new_states:
            self.states.add(state)

    def proceed(self, trigger):
        succes = False
        next_state = self.current_state.next(trigger)
        print('{} --- {} --- > {}'.format(self.current_state, trigger, next_state), end='::')
        if next_state and next_state in self.states:
            self.current_state = next_state
            succes = True
        print(succes)
        return self.current_state, succes

    @classmethod
    def build(cls, name: str, current_state: State):
        machine = cls.machines[name]
        machine.current_state = current_state
        return machine

    def __repr__(self):
        return {
            'name': self.name,
            'current_state': self.current_state.__str__()
        }

